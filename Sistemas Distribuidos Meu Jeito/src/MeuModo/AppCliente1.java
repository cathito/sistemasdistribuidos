package MeuModo;


import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Scanner;

public class AppCliente1 {

    private static Cliente cliente;

//====================================================================================================================//

    public static void main(String[] args) throws Exception {

        Scanner input = new Scanner(System.in);

     //================================================================================================//

        System.out.println("                Olá, Seja Bem Vindo(A)!\n");
        System.out.print("Antes de Inicarmos, Gostaria de Saber Qual seu nome: ");
        String nomeUsuario = input.nextLine();

        System.out.print("Informe a Porta na Qual o Socket Vai Ouvir: ");
        int ouvinadoNaPorta = input.nextInt();

        cliente = new Cliente( nomeUsuario, ouvinadoNaPorta );
        System.out.println("\n===================================================================\n");

        cliente.registarseNoServidor();
        System.out.println("===================================================================\n");

     //===============================================================================================//


        while(true) {
         //===============================================================================================//
                // servidor deve listar
                //Collection<Mensagem> cMensagens = Protocolo.receberMensagens(cartaEnviar);
         //===============================================================================================//

            System.out.println("           "+nomeUsuario + ", Qual Ação Voce Deseja Realizar?\n");
            System.out.println("                     Menu de Opcoes:            ");
            System.out.println("                (E) - Envio de Mensagem!        ");
            System.out.println("              (R) - Recebineto de Mensagem!     ");
            System.out.println("                       (S) - Sair!              ");
            System.out.println("===================================================================\n");

            System.out.print("Realizar: ");
            String opcao = input.next().toUpperCase();

            if( !opcao.equals("E") && !opcao.equals("R") && !opcao.equals("S") ){
                System.out.println("                              Atenção Opção Invalida!\n " +
                                   "      Solicitamos que Especifique Uma das Opçoes as Pertencente do Menu!\n");
                continue;

            }else if( opcao.equals("S") ){
                cliente.excluirRegistaroNoServidor();
                System.out.println("Finalizando Aplicação...");
                break;

            }else if (opcao.equals("E")) { //receberMensagem
                System.out.print("Informe o Endereco IP do Destinatario: ");
                String ipDestinatario = input.next();

                System.out.print("Informe a Porta Que o Destinatario Esta Ouvindo: ");
                int destinatarioOuveNaPorta = input.nextInt();
                cliente.setDadosParceiro( new Registro(ipDestinatario, destinatarioOuveNaPorta) );

                System.out.println("             Chat Configurado Com Sucesso!");
                cliente.enviarPrimeiraMensagem();

            }else {
                System.out.println("             Chat Configurado Com Sucesso!");
            }


         //-----------------------------------------------//

            byte[] reciveDataC = new byte[1024];
            DatagramPacket recivedPacket;
            Mensagem recivMsg;

         //===============================================================================================//
            boolean start = true;
            while(true){ // Primeiro OUVE a mensagem, para depoi ENVIAR a mensagem de resposta
                Scanner inputConversa = new Scanner(System.in);
                System.out.println("\n    Para Finalizar a Conversa Digite: 'Sair'\n");
/*
                if(start && !(opcao.equals("R"))){
                    System.out.print(InetAddress.getLocalHost().getHostAddress() + " | " + cliente.getDadosCliente().getNome() + ": ");
                    String mensagem = inputConversa.nextLine();

                    Mensagem envelopMensagem = new Mensagem(1, cliente.getDadosCliente().getNome(), cliente.getDadosParceiro().getIp(),
                            cliente.getDadosParceiro().getPorta(), mensagem,0, "ok");

                    Protocolo.enviarMensagens(envelopMensagem, cliente.getLocalSocket());
                    start=false;
                }*/
                recivedPacket = Protocolo.receberDatagramaMensagem(cliente.getLocalSocket());
                recivMsg = Protocolo.extarairMensagem(recivedPacket);

                if(recivMsg.getCodigoOperacao()==1){
                    cliente.setDadosParceiro(new Registro(recivMsg.getNomeUsuario(), recivMsg.getIpDestino(), recivedPacket.getPort()));
                }else if(recivMsg.getCodigoOperacao()==5){
                    System.out.println("Mensagem recebida de " + cliente.getDadosParceiro().getIp()+ " | " + recivMsg.getNomeUsuario() + ": Desconectou-se! :(");
                    cliente.setDadosParceiro(null);
                    System.out.println("===================================================================");
                    break;
                }

                System.out.println("Mensagem recebida de " + cliente.getDadosParceiro().getIp() + " | " + recivMsg.getNomeUsuario() + ": " + recivMsg.getTexto());


                System.out.print(InetAddress.getLocalHost().getHostAddress() + " | " + nomeUsuario + ": ");
                String mensagem = inputConversa.nextLine();

                if (mensagem.toUpperCase().equals("SAIR")) {
                    Mensagem envelopMensagem = new Mensagem(5, cliente.getDadosCliente().getNome(), cliente.getDadosParceiro().getIp(),
                            cliente.getDadosParceiro().getPorta(), mensagem,0, "ok");
                    Protocolo.enviarMensagens(envelopMensagem, cliente.getLocalSocket());
                    cliente.setDadosParceiro(null);
                    System.out.println("===================================================================");
                    break;
                }

                Mensagem envelopMensagem = new Mensagem(3, cliente.getDadosCliente().getNome(), cliente.getDadosParceiro().getIp(),
                        cliente.getDadosParceiro().getPorta(), mensagem,0, "ok");

                Protocolo.enviarMensagens(envelopMensagem, cliente.getLocalSocket());

                //
            }

         //===============================================================================================//


        }

    }

}
