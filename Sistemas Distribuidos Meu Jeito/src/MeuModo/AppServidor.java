package MeuModo;


import java.net.DatagramPacket;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Scanner;

public class AppServidor {

    public static Servidor servidor;

//================================================================================================//

    public static void main(String[] args) throws Exception {
        Scanner ler = new Scanner(System.in);

        System.out.println("              Configurando Servidor...\n");

        System.out.print("Porta Responsavel Por Receber os Dados: ");
        int listInPort = ler.nextInt();

        servidor = new Servidor(listInPort);

        System.out.println("\n            Servidor Configurando Com Sucesso!            ");
        System.out.println(  "==========================================================");
        System.out.println("\nAguardando Cliente...");

     //=====================================================================================//

        byte[] reciveDataC = new byte[1024];
        DatagramPacket recivedPacket;
        Mensagem recivMsg;


        while (true) {
            recivedPacket = Protocolo.receberDatagramaMensagem(servidor.getServerSocket());
            recivMsg = Protocolo.extarairMensagem(recivedPacket);

            int cOp = recivMsg.getCodigoOperacao();
            String nomeUsuario = recivMsg.getNomeUsuario();
            String ipCliente = recivedPacket.getAddress().getHostAddress();
            int portaCliente = recivedPacket.getPort();
            String camposRegistro = recivMsg.getTexto();
            System.out.println(camposRegistro);

            if (cOp == 1) {
                servidor.registrarCliente(nomeUsuario, new Registro(nomeUsuario,ipCliente,portaCliente));
                Protocolo.enviarMensagens(servidor.obterMensagemRegistros(), servidor.getServerSocket(), ipCliente, portaCliente);
            } else if (cOp == 2) {
                servidor.desregistrarCliente(nomeUsuario);
            }else if (cOp == 0) {
                Protocolo.enviarMensagens((Collection) servidor.hRegistros, servidor.getServerSocket(), ipCliente, portaCliente);
            }

            servidor.listarRegistros();

            System.out.println("Servidor Aguardando Proximo Cliente...");
            System.out.println("==========================================================");
        }


     //=====================================================================================// 32:49

    }

}
