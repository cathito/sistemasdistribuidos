package MeuModo;


import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {
    private Registro dadosCliente = null;
    private Registro dadosParceiro = null;
    private Registro dadosServidor = new Registro();
    private DatagramSocket localSocket;

//=============================================================================================================//

    public Cliente() throws SocketException {
        localSocket = new DatagramSocket();
    }

    public Cliente(String nome) throws SocketException, UnknownHostException {
        this();
        this.dadosCliente = new Registro(nome, InetAddress.getLocalHost().toString(), localSocket.getPort());
    }

    public Cliente(String nome, int portaLocal) throws UnknownHostException {
        boolean socketCriado = false;

        while(!socketCriado){
            try {
                localSocket = new DatagramSocket(portaLocal);
                socketCriado=true;
                System.out.println("Socket Server Criado na Porta ("+ portaLocal+").");
            }catch(Exception e){
                System.out.println("Falha o Criar Socket Server");
                System.out.println("Incrementadndo a porta e tentado novamente...");
                portaLocal++;
            }
        }
        dadosCliente = new Registro(nome, InetAddress.getLocalHost().getHostAddress(), portaLocal);
    }

    public Cliente(String nome, DatagramSocket cClienteSocket) {
        this.dadosCliente = new Registro(nome, String.valueOf(cClienteSocket.getInetAddress()), cClienteSocket.getLocalPort());
        localSocket = cClienteSocket;
    }

    public Cliente(String nomeUsuario, String ipParceiro, int portaParceiro) throws SocketException, UnknownHostException {
        this(nomeUsuario); // obs muito Simplificado
        this.dadosParceiro = new Registro("Parceiro", ipParceiro, portaParceiro);
    }

//=============================================================================================================//

    public void registarseNoServidor(){
        Scanner input = new Scanner(System.in);

        System.out.println("        Por favor informe os dados do Servidor de Registro\n");
        System.out.print(  "Informe o Endereco IP do Servidor de Registro:");
        String ipServidorRegistro = input.next();

        System.out.print(  "Informe a Porta Que o Servidor de Registro Esta Ouvindo: ");
        int servidorRegistroOuveNaPorta = input.nextInt();

        setDadosServidor( new Registro(ipServidorRegistro, servidorRegistroOuveNaPorta ) );

        Mensagem mensagem = new Mensagem(1, dadosCliente.getNome(), ipServidorRegistro, servidorRegistroOuveNaPorta);

        Protocolo.enviarMensagens(mensagem, localSocket);
        System.out.println("        Registro Realizado Com Successo!");
    }

    public void excluirRegistaroNoServidor(){
        Mensagem mensagem = new Mensagem(2, dadosCliente.getNome(), dadosServidor.getIp(), dadosServidor.getPorta());

        Protocolo.enviarMensagens(mensagem, localSocket);
        System.out.println("    Usuario Desregistrado do Servidor Com Successo!");
    }

    public boolean enviarPrimeiraMensagem () throws UnknownHostException {
        Scanner input = new Scanner(System.in);

        System.out.print(InetAddress.getLocalHost().getHostAddress() + " | " + dadosCliente.getNome() + ": ");
        String mensagem = input.nextLine();

        Mensagem envelopMensagem = new Mensagem(1, dadosCliente.getNome(), dadosParceiro.getIp(),
                dadosParceiro.getPorta(), mensagem,0, "ok");

        Protocolo.enviarMensagens(envelopMensagem, localSocket);
        return true;
    }

    public boolean enviarMensagem (String nome, int portaLocal){

        return true;
    }

    public boolean ReceberMensagem (){

        return true;
    }

//=============================================================================================================//

    public Registro getDadosCliente() {
        return dadosCliente;
    }

    public void setDadosCliente(Registro dadosCliente) {
        this.dadosCliente = dadosCliente;
    }

    public Registro getDadosParceiro() {
        return dadosParceiro;
    }

    public void setDadosParceiro(Registro dadosParceiro) {
        this.dadosParceiro = dadosParceiro;
    }

    public DatagramSocket getLocalSocket() {
        return localSocket;
    }

    public void setLocalSocket(DatagramSocket localSocket) {
        this.localSocket = localSocket;
    }

    public Registro getDadosServidor() {
        return dadosServidor;
    }

    public void setDadosServidor(Registro dadosServidor) {
        this.dadosServidor = dadosServidor;
    }

    //=============================================================================================================//

}
