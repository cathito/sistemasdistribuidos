package MeuModo;

import java.net.DatagramSocket;
import java.util.Collection;

public interface IServidorRegistro {                                 //========== OK ==========//

    DatagramSocket startServer(int listrunnigPort);
    boolean clienteRegistrado(String nome);
    boolean registrarCliente(String nome, String ip, int porta);
    boolean registrarCliente(String nome, Registro registro);
    boolean desregistrarCliente(String nome);
    Registro obterRegistro(String nome);
    void listarRegistros();
    Collection obterMensagemRegistros();

}
