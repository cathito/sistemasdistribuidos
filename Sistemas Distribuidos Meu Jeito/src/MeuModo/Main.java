package MeuModo;

import java.util.Hashtable;

public class Main {
    public static void main(String[] args) {

        Registro r1 = new Registro("Joao", "120.0.0.1", 5000);
        Registro r2 = new Registro("te", "120.0.0.1", 5000);
        Registro r3 = new Registro("rutefor", "120.0.0.1", 5000);
        Registro r4 = new Registro("Bor", "120.0.0.1", 5000);
        Registro r5 = new Registro("Bore", "120.0.0.1", 5000);

        Hashtable<String, Registro> hRegistros = new Hashtable<String, Registro>();
        hRegistros.put("Joao",r1);
        hRegistros.put("te",r2);
        hRegistros.put("rutefor",r3);
        hRegistros.put("Bor",r4);

        if(hRegistros.contains(r1)){
            System.out.println("contem Objeto");
        }

        if(hRegistros.containsKey("Joao") ){
            System.out.println("contem Joao");
        }
        System.out.println("==========================================================");
        System.out.println(r1);
        System.out.println("==========================================================");

        for (Object key : hRegistros.keySet()) {
            System.out.println(hRegistros.get(key).getStringRegistro());
        }
    }
}
