package MeuModo;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Mensagem {                                 //========== OK ==========//
    public static final int MSG_STATUS = 0;
    public static final int MSG_REGISTRO = 1;
    public static final int MSG_DESREGISTRO = 2;
    public static final int MSG_RESPOSTA_UNICA = 3;
    public static final int MSG_RESPOSTA_MULTIPLA = 4;
    public static final int MSG_FIM_RESPOSTA = 5;

    private int codigoOperacao = 1;
    private String nomeUsuario = "";
    private String ipDestino = "127.0.0.1";
    private int portaDestino = 4444;
    private String texto = "";
    private int status = 0;
    private String descrStatus = "";

//=============================================================================================================//

    public Mensagem(){
        this.descrStatus = "ok";
    }

    public Mensagem(int codigoOperacao, String nomeUsuario, String ipDestino, int portaDestino){
        this();
        this.codigoOperacao = codigoOperacao;
        this.nomeUsuario = nomeUsuario;
        this.ipDestino = ipDestino;
        this.portaDestino = portaDestino;
    }

    public Mensagem(int codigoOperacao, String nomeUsuario, String ipDestino,int portaDestino, String texto){
        this(codigoOperacao,nomeUsuario,ipDestino,portaDestino);
        this.texto = texto;
    }

    public Mensagem(int codigoOperacao, String nomeUsuario, String ipDestino,int portaDestino, String texto, int status, String descrStatus){
        this(codigoOperacao,nomeUsuario,ipDestino,portaDestino, texto);
        this.status = status;
        this.descrStatus = descrStatus;
    }

    public Mensagem(String conjuntoDeCampos) throws Exception {
        String[] campos = conjuntoDeCampos.split(";");

        if((campos.length) != 7){
            System.out.println("Tamanho Mensagem: "+campos.length);
            throw new Exception("Formato Invalido de Mensagem!");
        }else{
            codigoOperacao = Integer.valueOf(campos[0]).intValue();
            nomeUsuario = campos[1];
            ipDestino = campos[2];
            portaDestino = Integer.valueOf(campos[3]).intValue();
            texto = campos[4];
            status = Integer.valueOf(campos[5]).intValue();
            descrStatus = campos[6];
        }
    }

//=============================================================================================================//

    public String getStringMessage() {
        return codigoOperacao + ";" + nomeUsuario + ";" + ipDestino + ";" + portaDestino + ";" + texto + ";" + status + ";" + descrStatus+ ";";
    }
//=============================================================================================================//

    public int getCodigoOperacao() {
        return codigoOperacao;
    }

    public void setCodigoOperacao(int codigoOperacao) {
        this.codigoOperacao = codigoOperacao;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getIpDestino() throws UnknownHostException {
        return ipDestino;
    }

    public void setIpDestino(String ipDestino) {
        this.ipDestino = ipDestino;
    }

    public int getPortaDestino() {
        return portaDestino;
    }

    public void setPortaDestino(int portaDestino) {
        this.portaDestino = portaDestino;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescrStatus() {
        return descrStatus;
    }

    public void setDescrStatus(String descrStatus) {
        this.descrStatus = descrStatus;
    }
//=============================================================================================================//


}
