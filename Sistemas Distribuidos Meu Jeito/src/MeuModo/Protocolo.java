package MeuModo;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Protocolo {                                 //========== OK ==========//

//=============================================================================================================//

    public static boolean enviarMensagens(Mensagem msg, DatagramSocket localSocket){
        try {
            byte[] sendDataC = msg.getStringMessage().getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendDataC, sendDataC.length, InetAddress.getByName(msg.getIpDestino()), msg.getPortaDestino());
            localSocket.send(sendPacket);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }


    public static Mensagem receberMensagem(DatagramSocket localSocket) throws Exception {
        byte[] reciveDataC = new byte[1024];
        DatagramPacket recivePacket = new DatagramPacket(reciveDataC, reciveDataC.length);
        localSocket.receive(recivePacket);
        String reciveMsg = new String(recivePacket.getData()).trim();
        Mensagem msg = new Mensagem(reciveMsg);
        return msg;
    }

    public static DatagramPacket receberDatagramaMensagem(DatagramSocket localSocket) throws Exception {
        byte[] reciveDataC = new byte[1024];
        DatagramPacket reciveDatagramPacket = new DatagramPacket(reciveDataC, reciveDataC.length);
        localSocket.receive(reciveDatagramPacket);
        String reciveMsg = new String(reciveDatagramPacket.getData()).trim();
        return reciveDatagramPacket;
    }

    public static Mensagem extarairMensagem(DatagramPacket recivedPacket) throws Exception {
        String revidMsg = new String(recivedPacket.getData()).trim();
        Mensagem msg = new Mensagem(revidMsg);
        return msg;
    }

    // para parte 2 apartir do 26:30
//=============================================================================================================//

    public static Collection receberMensagens(DatagramSocket clienteSocket) throws Exception {
        Collection <Mensagem> cMensagens = new ArrayList();

        byte[] reciveDataC = new byte[1024];
        Mensagem recMsg;

        DatagramPacket recivePacket = null;
        String strRecivMsg = "";
        Mensagem msgStatus = null;

        int codOperacao = Mensagem.MSG_RESPOSTA_MULTIPLA;

        do{
            strRecivMsg = "";
            reciveDataC = new byte[1024];
            recivePacket = new DatagramPacket(reciveDataC, reciveDataC.length);
            clienteSocket.receive(recivePacket);
            strRecivMsg = new String(recivePacket.getData()).trim();
            recMsg = new Mensagem(strRecivMsg);
            codOperacao = recMsg.getCodigoOperacao();

            cMensagens.add(recMsg);

            if(codOperacao != Mensagem.MSG_FIM_RESPOSTA){
                msgStatus = new Mensagem(0, " ", recivePacket.getAddress().getHostAddress(), recivePacket.getPort(),"",0, "--");
                Protocolo.enviarMensagens(msgStatus, clienteSocket);
            }
        }while(codOperacao != Mensagem.MSG_FIM_RESPOSTA);

        return cMensagens;
    }

    public static boolean enviarMensagens(Collection cMensagens, DatagramSocket clienteSocket, String peerHostName, int peerHostporta) {

        try {
            Iterator iMensagens = cMensagens.iterator();
            int qtdMsgFila = cMensagens.size();
            int codOperacao = 4;
            int seqMsg= 1;

            if(seqMsg==qtdMsgFila){
                codOperacao = 5;
            }

            Mensagem msgToSend = (Mensagem) iMensagens.next();
            msgToSend.setCodigoOperacao(codOperacao);
            msgToSend.setNomeUsuario("Servidor");
            msgToSend.setIpDestino(peerHostName);
            msgToSend.setPortaDestino(peerHostporta);

            Protocolo.enviarMensagens(msgToSend, clienteSocket);

            Mensagem revdMsg = null;

            while (iMensagens.hasNext()){
                revdMsg = Protocolo.receberMensagem(clienteSocket);
                if(revdMsg.getCodigoOperacao() == 0){
                    msgToSend = (Mensagem) iMensagens.next();
                    ++seqMsg;
                    if(seqMsg==qtdMsgFila){
                        codOperacao = 5;
                    }

                    msgToSend.setCodigoOperacao(codOperacao);
                    msgToSend.setNomeUsuario("Servidor");
                    msgToSend.setIpDestino(peerHostName);
                    msgToSend.setPortaDestino(peerHostporta);

                    Protocolo.enviarMensagens(msgToSend, clienteSocket);

                }else{
                    throw new Exception("Falha na Mensagem de Retorno");
                }
            }

        }catch (Exception e){
            return false;
        }
        return true;
    }
//=============================================================================================================//

}
