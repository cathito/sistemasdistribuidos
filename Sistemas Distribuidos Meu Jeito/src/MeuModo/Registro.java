package MeuModo;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Registro {
    private String nome = "";
    private String ip = "127.0.0.1";
    private int porta = 4444;

//=============================================================================================================//

    public Registro() { }

    public Registro(String ip, int porta) {
        this.porta = porta;
        this.ip = ip;
    }

    public Registro(String nome, String ip, int porta) {
        this(ip, porta);
        this.nome = nome;
    }

    public Registro(String conjuntoDeCampos) throws Exception {
        String[] campos = conjuntoDeCampos.split(";");

        if((campos.length) != 3){
            throw new Exception("Formato Invalido de Mensagem!");
        }else{
            nome = campos[0];
            ip = campos[1];
            porta = Integer.valueOf(campos[2]).intValue();
        }
    }

//=============================================================================================================//

    public String getStringRegistro() {
        return nome + ";" + ip + ";" + String.valueOf(porta) + ";";
    }

//=============================================================================================================//

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

//=============================================================================================================//

}
