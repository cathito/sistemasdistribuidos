package MeuModo;


import java.net.DatagramSocket;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;

public class Servidor implements IServidorRegistro {
    private int porta;
    Hashtable<String, Registro> hRegistros = new Hashtable<String, Registro>();
    DatagramSocket serverSocket;

//=============================================================================================================//

    public Servidor(int porta){
        startServer(porta);
    }

//=============================================================================================================//
    @Override
    public DatagramSocket startServer(int listrunnigPort) {
        boolean serverStarted = false;
        while(!serverStarted){
            try {
                serverSocket = new DatagramSocket(listrunnigPort);
                serverStarted=true;
                System.out.println("Socket Server Ouvindo na Porta ("+ listrunnigPort+").");

            }catch(Exception e){
                System.out.println("Falha o Ativar o Socket Server");
                System.out.println("Incrementadndo a porta e tentado novamente...");
                listrunnigPort++;
            }
        }
        this.porta = listrunnigPort;
        return serverSocket;
    }

    @Override
    public boolean clienteRegistrado(String nome) {
        return this.gethRegistros().containsKey(nome);
    }

    @Override
    public boolean registrarCliente(String nome, String ip, int porta) {
        if(!clienteRegistrado(nome)){

        }
        return false;
    }

    @Override
    public boolean registrarCliente(String nome, Registro registro) {

        hRegistros.put(nome,registro);

        return true;
    }

    @Override
    public boolean desregistrarCliente(String nome) {
        if(hRegistros.containsKey(nome) ){
            hRegistros.remove(nome);
        }
        return true;
    }

    @Override
    public Registro obterRegistro(String nome) {
        return null;
    }

    @Override
    public void listarRegistros() {
        System.out.println("----------------------------------------------------------");
        System.out.println("                    Clientes Registros!                   ");
        for (Object key : hRegistros.keySet()) {
            System.out.println("   >> "+hRegistros.get(key).getStringRegistro());
        }
        System.out.println("----------------------------------------------------------\n");
    }

    @Override
    public Collection obterMensagemRegistros() {
        return null;
    }
//=============================================================================================================//


    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

    public Hashtable gethRegistros() {
        return hRegistros;
    }

    public void sethRegistros(Hashtable hRegistros) {
        this.hRegistros = hRegistros;
    }

    public DatagramSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(DatagramSocket serverSocket) {
        this.serverSocket = serverSocket;
    }
}
