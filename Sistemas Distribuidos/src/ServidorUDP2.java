import java.io.IOException;
import java.net.*;
import java.util.*;

public class ServidorUDP2 {

    public static void main(String[] args) throws Exception {

        DatagramSocket tomadaServidora = new DatagramSocket(5000);
        System.out.println("Servidor em execução!");

        while(true) {

            ///////////RECEBER MENSAGEM DO CLIENTE E IMPRIMIR NA TELA
            byte[] cartaAReceber = new byte[100];
            DatagramPacket envelopeAReceber = new DatagramPacket(cartaAReceber, cartaAReceber.length);

            tomadaServidora.receive(envelopeAReceber);
            String textoRecebido = new String(envelopeAReceber.getData());
            System.out.println("Mensagem recebida de " + textoRecebido);

            ///////////ENVIAR MENSAGEM DE VOLTA AO CLIENTE
            Scanner input = new Scanner(System.in);
            byte[] cartaAEnviar = new byte[100];
            System.out.print(InetAddress.getLocalHost().getHostAddress() + " | Servidor:");
            String mensagem = input.nextLine();
            cartaAEnviar = mensagem.getBytes();

            InetAddress ipCliente = envelopeAReceber.getAddress();
            int portaCliente = envelopeAReceber.getPort();

            DatagramPacket envelopeAEnviar = new DatagramPacket(cartaAEnviar, cartaAEnviar.length, ipCliente, portaCliente);
            tomadaServidora.send(envelopeAEnviar);
        }

    }
}
