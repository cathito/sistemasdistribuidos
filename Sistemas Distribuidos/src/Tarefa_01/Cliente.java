package Tarefa_01;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Cliente {
    private Registro dadosCliente = null;
    private Registro dadosParceiro = null;
    private DatagramSocket localSocket;


//=============================================================================================================//

    public Cliente() throws SocketException {
        localSocket = new DatagramSocket();
    }

    public Cliente(String nome) throws SocketException, UnknownHostException {
        this();
        this.dadosCliente = new Registro(nome, InetAddress.getLocalHost().toString(), localSocket.getPort());
    }

    public Cliente(String nome, int portaLocal) throws UnknownHostException {
        boolean socketCriado = false;

        while(!socketCriado){
            try {
                localSocket = new DatagramSocket(portaLocal);
                socketCriado=true;
                System.out.println("Socket Server Criado na Porta "+ portaLocal);
            }catch(Exception e){
                System.out.println("Falha o Criar Socket Server");
                System.out.println("Incrementadndo a porta e tentado novamente...");
                portaLocal++;
            }
        }
        dadosCliente = new Registro(nome, InetAddress.getLocalHost().toString(), portaLocal);
    }

    public Cliente(String nome, DatagramSocket cClienteSocket) {
        this.dadosCliente = new Registro(nome, String.valueOf(cClienteSocket.getInetAddress()), cClienteSocket.getLocalPort());
        localSocket = cClienteSocket;
    }

    public Cliente(String nomeUsuario, String ipParceiro, int portaParceiro) throws SocketException, UnknownHostException {
        this(nomeUsuario); // obs muito Simplificado
        this.dadosParceiro = new Registro("Parceiro", ipParceiro, portaParceiro);
    }

//=============================================================================================================//

    public boolean enviarPrimeiraMensagem (){

        return true;
    }

    public boolean enviarMensagem (String nome, int portaLocal){

        return true;
    }

    public boolean ReceberMensagem (){

        return true;
    }

//=============================================================================================================//

    public Registro getDadosCliente() {
        return dadosCliente;
    }

    public void setDadosCliente(Registro dadosCliente) {
        this.dadosCliente = dadosCliente;
    }

    public Registro getDadosParceiro() {
        return dadosParceiro;
    }

    public void setDadosParceiro(Registro dadosParceiro) {
        this.dadosParceiro = dadosParceiro;
    }

    public DatagramSocket getLocalSocket() {
        return localSocket;
    }

    public void setLocalSocket(DatagramSocket localSocket) {
        this.localSocket = localSocket;
    }

//=============================================================================================================//

}
