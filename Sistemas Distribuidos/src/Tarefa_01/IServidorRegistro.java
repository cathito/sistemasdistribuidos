package Tarefa_01;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Collection;

public interface IServidorRegistro {

    DatagramSocket startServer(int listrunnigPort);
    boolean clienteRegistrado(String nome);
    boolean registrarCliente(String nome,String ip, int porta);
    boolean registrarCliente(String nome,Registro registro);
    boolean desregistrarCliente(String nome,Registro registro);
    Registro obterRegistro(String nome);
    void listarRegistros();
    Collection obterMensagemRegistros();

}
