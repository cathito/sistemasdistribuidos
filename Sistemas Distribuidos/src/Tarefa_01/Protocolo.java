package Tarefa_01;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Protocolo {

//=============================================================================================================//

    public static boolean enviarMensagem(Mensagem msg, DatagramSocket localSocket){
        try {
            byte[] sendDataC = msg.getStringMessage().getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendDataC, sendDataC.length, msg.getIpDestino(), msg.getPortaDestino());
            localSocket.send(sendPacket);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }


    public static Mensagem receberMensagem(DatagramSocket localSocket) throws Exception {
        byte[] reciveDataC = new byte[1024];
        DatagramPacket recivePacket = new DatagramPacket(reciveDataC, reciveDataC.length);
        localSocket.receive(recivePacket);
        String reciveMsg = new String(recivePacket.getData()).trim();
        Mensagem msg = new Mensagem(reciveMsg);
        return msg;
    }

    public static DatagramPacket receberDatagramaMensagem(DatagramSocket localSocket) throws Exception {
        byte[] reciveDataC = new byte[1024];
        DatagramPacket reciveDatagramPacket = new DatagramPacket(reciveDataC, reciveDataC.length);
        localSocket.receive(reciveDatagramPacket);
        String reciveMsg = new String(reciveDatagramPacket.getData()).trim();
        Mensagem msg = new Mensagem(reciveMsg);
        return reciveDatagramPacket;
    }

    public static Mensagem extarairMensagem(DatagramPacket recivedPacket) throws Exception {
        String revidMsg = new String(recivedPacket.getData()).trim();
        Mensagem msg = new Mensagem(revidMsg);
        return msg;
    }

    // para parte 2 apartir do 26:30
//=============================================================================================================//


}
