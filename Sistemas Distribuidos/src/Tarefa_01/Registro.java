package Tarefa_01;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Registro {
    private String nome = "";
    private String ip = "127.0.0.1";
    private int porta = 4444;

//=============================================================================================================//
    public Registro() {

    }

    public Registro(String nome, String ip, int porta) {
        this.nome = nome;
        this.porta = porta;
        this.ip = ip;
    }

    public Registro(String aaa) {

    }

//=============================================================================================================//

    public String getStringRegistro() {
        return nome + ", " + ip + ", " + String.valueOf(porta);
    }

//=============================================================================================================//

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public InetAddress getIp() throws UnknownHostException {
        return InetAddress.getByName(ip);
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

//=============================================================================================================//

}
