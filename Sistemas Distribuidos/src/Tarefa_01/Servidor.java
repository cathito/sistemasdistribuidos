package Tarefa_01;

import java.net.DatagramSocket;
import java.util.Collection;
import java.util.Hashtable;

public class Servidor implements IServidorRegistro {
    private int porta;
    Hashtable hRegistros;
    DatagramSocket serverSocket;

//=============================================================================================================//

    public Servidor(){

    }

    public Servidor(int porta){
        this.porta = porta;
    }

//=============================================================================================================//
    @Override
    public DatagramSocket startServer(int listrunnigPort) {
        return null;
    }

    @Override
    public boolean clienteRegistrado(String nome) {
        return false;
    }

    @Override
    public boolean registrarCliente(String nome, String ip, int porta) {
        return false;
    }

    @Override
    public boolean registrarCliente(String nome, Registro registro) {
        return false;
    }

    @Override
    public boolean desregistrarCliente(String nome, Registro registro) {
        return false;
    }

    @Override
    public Registro obterRegistro(String nome) {
        return null;
    }

    @Override
    public void listarRegistros() {

    }

    @Override
    public Collection obterMensagemRegistros() {
        return null;
    }
//=============================================================================================================//

    public int getPorta() {
        return porta;
    }

    public Hashtable gethRegistros() {
        return hRegistros;
    }

    public DatagramSocket getServerSocket() {
        return serverSocket;
    }
}
