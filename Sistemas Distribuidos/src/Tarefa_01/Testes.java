package Tarefa_01;

public class Testes {
    public static void main(String[] args) {
        Mensagem m = new Mensagem();
        System.out.println(m.toString());

        Mensagem m2 = new Mensagem(1,"Joao","127.0.0.1", 4);
        System.out.println(m2.toString());

        Mensagem m3 = new Mensagem(1,"Carlos","127.0.0.2", 4, "Texto");
        System.out.println(m3.toString());

    }
}
