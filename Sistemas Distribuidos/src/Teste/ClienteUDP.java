package Teste;

import java.io.IOException;
import java.net.*;
import java.util.*;

public class ClienteUDP{

    public static void main(String[] args) throws IOException {

        Scanner input = new Scanner(System.in);
        String IP_Destinatario = "127.0.0.1";
        String nomeUsuario;

//========================================================================================//
        System.out.println("                Olá, Seja Bem Vindo(A)!\n");
          System.out.print("Antes de Inicarmos, Gostaria de Saber Qual seu nome:");
        nomeUsuario = input.next();

//========================================================================================//
        while(true) {
            System.out.println("\n                - Para finalizar digite 'Sair'! -\n");
            DatagramSocket conexaoServidor = new DatagramSocket();

            System.out.print("Enderreco IP do Destinatario:"+IP_Destinatario);
            System.out.println();
            //IP_Destinatario = input.next();

            if (IP_Destinatario.toUpperCase().equals("SAIR")) {
                break;
            }
        //-----------------------------------------------------------------------------------------------//
            boolean contatoInicial = true;

            while (true) {

                /*if(contatoInicial){
                    byte[] cartaAEnviar = new byte[100];
                    cartaAEnviar = IP_Destinatario.getBytes();
                    InetAddress ip = InetAddress.getByName("127.0.0.1");

                    DatagramPacket envelopeAEnviar = new DatagramPacket(cartaAEnviar, cartaAEnviar.length, ip, 5000);
                    conexaoServidor.send(envelopeAEnviar);


                    byte[] cartaAReceber = new byte[100];
                    DatagramPacket envelopeAReceber = new DatagramPacket(cartaAReceber, cartaAReceber.length);
                    conexaoServidor.receive(envelopeAReceber);
                    String mensagemRecebida = new String(envelopeAReceber.getData());
                }*/

                Scanner inputConversa = new Scanner(System.in);
                System.out.print(InetAddress.getLocalHost().getHostAddress() + " | " + nomeUsuario + ":");
                String mensagem = inputConversa.nextLine();

                if (mensagem.toUpperCase().equals("SAIR")) {
                    break;
                }

                byte[] cartaAEnviar = new byte[100];
                cartaAEnviar = (InetAddress.getLocalHost().getHostAddress() + " | " + nomeUsuario + ": " + mensagem).getBytes();
                InetAddress ip = InetAddress.getByName(IP_Destinatario);

                DatagramPacket envelopeAEnviar = new DatagramPacket(cartaAEnviar, cartaAEnviar.length, ip, 5000);
                conexaoServidor.send(envelopeAEnviar);


                byte[] cartaAReceber = new byte[100];
                DatagramPacket envelopeAReceber = new DatagramPacket(cartaAReceber, cartaAReceber.length);
                conexaoServidor.receive(envelopeAReceber);
                String mensagemRecebida = new String(envelopeAReceber.getData());


                System.out.println("Mensagem recebida de " + mensagemRecebida);
            }

            conexaoServidor.close();
            System.out.println("                    ...Conexao Encerrada!");
        //-----------------------------------------------------------------------------------------------//

        }
    }
}
