package Teste;

import java.io.IOException;
import java.net.*;
import java.util.*;


public class ServidorUDP {

    public static DatagramPacket mensagemResposta(DatagramPacket envelopeAReceber, String mensageminput) throws IOException {
        byte[] cartaAEnviar = new byte[100];

        if(mensageminput.toUpperCase().equals("SAIR")){
            cartaAEnviar = ("Conexao Encerrada!").getBytes();
        }else {
            cartaAEnviar = (InetAddress.getLocalHost().getHostAddress() + " | Servidor:" + mensageminput).getBytes();
        }

        InetAddress ipCliente = envelopeAReceber.getAddress();
        int portaCliente = envelopeAReceber.getPort();

        DatagramPacket envelopeAEnviar = new DatagramPacket(cartaAEnviar,  cartaAEnviar.length, ipCliente, portaCliente);
        return envelopeAEnviar;
    }

    public static void main(String[] args) throws IOException {

        DatagramSocket datagramSocket = new DatagramSocket(5000);
        InetAddress ipDestinatario = null;


        System.out.println("Servidor em execução...");

        while(true) {
            ///////////RECEBER MENSAGEM DO CLIENTE E IMPRIMIR NA TELA
            byte[] cartaAReceber = new byte[100];
            DatagramPacket envelopeAReceber = new DatagramPacket(cartaAReceber, cartaAReceber.length);

            datagramSocket.receive(envelopeAReceber);
            String textoRecebido = new String(envelopeAReceber.getData());

            /*if (ipDestinatario == null) {
                ipDestinatario = InetAddress.getByName(textoRecebido);
            } else {*/
                System.out.println("Mensagem recebida de " + textoRecebido);


                ///////////ENVIAR MENSAGEM DE VOLTA AO CLIENTE
                Scanner input = new Scanner(System.in);
                System.out.print(InetAddress.getLocalHost().getHostAddress() + " | Servidor:");
                String mensageminput = input.nextLine();

                byte[] cartaAEnviar = new byte[100];
                InetAddress ipCliente = envelopeAReceber.getAddress();
                int portaCliente = envelopeAReceber.getPort();

                if (mensageminput.toUpperCase().equals("SAIR")) {
                    cartaAEnviar = ("Conexao Encerrada Por " + InetAddress.getLocalHost().getHostAddress() + " | Servidor!").getBytes();
                    DatagramPacket envelopeAEnviar = new DatagramPacket(cartaAEnviar, cartaAEnviar.length, ipCliente, portaCliente);
                    datagramSocket.send(envelopeAEnviar);
                    break;
                }

                cartaAEnviar = (InetAddress.getLocalHost().getHostAddress() + " | Servidor:" + mensageminput).getBytes();
                DatagramPacket envelopeAEnviar = new DatagramPacket(cartaAEnviar, cartaAEnviar.length, ipCliente, portaCliente);
                datagramSocket.send(envelopeAEnviar);
            //}

        }

        datagramSocket.close();

    }

}
